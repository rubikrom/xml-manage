package dev.guzh.xml;

import java.io.File;
import java.io.StringWriter;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class XMLObjectRegistry {
	
	@XmlElementDecl(namespace = "http://example.com", name = "person")
    public JAXBElement<Person> createPerson(Person value) {
        return new JAXBElement<Person>(new QName("http://example.com", "person"), Person.class, null, value);
    }

    @XmlElementDecl(namespace = "http://example.com", name = "address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(new QName("http://example.com", "address"), Address.class, null, value);
    }

    @SuppressWarnings("unchecked")
	public static void main(String[] args) {

        // Crear una instancia de Person
        Person person = new Person();
        person.setName("John Doe");
        person.setAge(30);

        // Crear una instancia de Address
        Address address = new Address();
        address.setStreet("123 Main St");
        address.setCity("New York");
        address.setCountry("USA");
        
        // Serializar Objetos Java a XML
        try {
        	// Crear instancia de JXBContext
        	JAXBContext context = JAXBContext.newInstance(XMLObjectRegistry.class, Person.class, Address.class);
        	// Crear un Marshaller para serializar
        	Marshaller marshaller = context.createMarshaller();
        	marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        	// Crear un JAXBElemento para Person
        	XMLObjectRegistry registry = new XMLObjectRegistry();
        	JAXBElement<Person> personElement = registry.createPerson(person);
        	JAXBElement<Address> addressElement = registry.createAddress(address);
        	// Serializar a XML
        	marshaller.marshal(personElement, new File("person.xml"));
        	marshaller.marshal(addressElement, new File("address.xml"));
        	// Serializar a String
        	String personString = marshallToString(marshaller, personElement);
        	System.out.println(personString);
        	String addressString = marshallToString(marshaller, addressElement);
        	System.out.println(addressString);
        	
        	
        }catch(JAXBException e) {
        	e.printStackTrace();
        }
        
        // Deserializar XML a Objetos Java
        try {
        	// Crear instancia de JXBContext
        	JAXBContext context = JAXBContext.newInstance(XMLObjectRegistry.class, Person.class, Address.class);
        	// Crear un Unmarshaller
        	Unmarshaller unmarshaller = context.createUnmarshaller();
        	// Deserializar el archivo XML de Person
        	JAXBElement<Person> personElement = (JAXBElement<Person>) unmarshaller.unmarshal(new File("person.xml"));
        	Person deserializedPerson = personElement.getValue();
        	System.out.println(deserializedPerson);
        	// Deserializar el archivo XML de Address
        	JAXBElement<Address> addressElement = (JAXBElement<Address>) unmarshaller.unmarshal(new File("address.xml"));
        	Address deserializedAddress = addressElement.getValue();
        	System.out.println(deserializedAddress);
        	
        }catch(JAXBException e) {
        	e.printStackTrace();
        }
    }
    
    private static String marshallToString(Marshaller marshaller, JAXBElement<?> element) throws JAXBException {
    	StringWriter writer = new StringWriter();
    	marshaller.marshal(element, writer);
    	return writer.toString();
    }
}
